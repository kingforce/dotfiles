import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List
from libqtile.widget import KeyboardLayout
from libqtile import qtile
from qtile_extras import widget
from qtile_extras.widget.decorations import PowerLineDecoration
from qtile_extras.widget.decorations import BorderDecoration

mod = "mod4"
terminal = "alacritty"
browser = "firefox"
launcher = "rofi -show drun"
filemanager = "pcmanfm"
textedit = "geany"

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    
    # App keybindings
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch Terminal"),
    Key([mod], "b", lazy.spawn(browser), desc="Launch Browser"),
    Key([mod], "r", lazy.spawn(launcher), desc="Launch Rofi"),
    Key([mod], "f", lazy.spawn(filemanager), desc="Launch PCmanFM"),
    Key([mod], "t", lazy.spawn(textedit), desc="Launch Geany"),
    
    # Volume control
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")),
    Key([], "XF86AudioMute", lazy.spawn("amixer -c 0 -q set Master toggle")),
    Key([mod], "equal", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
    Key([mod], "minus", lazy.spawn("amixer -c 0 -q set Master 2dB-")),
    
    # Toogle layouts, quitting and reloading, and closing windows
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    
    # Brightness control
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl -q set +5%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl -q set 5%-")),
    
    # Logout menu (Oblogout)
    Key([mod, "shift"], "x", lazy.spawn("oblogout")),
    
    #Keyboard switching
    Key([mod, "shift"], "space", lazy.widget['keyboardlayout'].next_keyboard()),
]

groups = [Group("", layout='monadtall'),
          Group("", layout='monadtall'),
          Group("", layout='monadtall'),
          Group("", layout='monadtall'),
          Group("", layout='monadtall'),
          Group("", layout='monadtall'),
          Group("", layout='monadtall'),
          Group("", layout='monadtall'),]

# Allow MODKEY+[0 through 9] to bind to groups, see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group
from libqtile.dgroups import simple_key_binder
dgroups_key_binder = simple_key_binder("mod4")

layouts = [
    layout.MonadTall(border_focus=["#282c34"], border_width=1, margin=5),
    layout.Columns(border_focus=["#282c34"], border_width=1, margin=5),
    layout.Max(border_focus=["#282c34"], border_width=1, margin=5),
    layout.Floating(border_focus=["#282c34"], border_width=1, margin=5)
    
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

    # Bar configuration starts here

widget_defaults = dict(
    font="Roboto Regular",
    fontsize=14,
    padding=5,
)
extension_defaults = widget_defaults.copy()

powerline1 = {"decorations": [PowerLineDecoration(path="arrow_right")]}
powerline2 = {"decorations": [PowerLineDecoration()]}

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
                background="#00dcaa",
                foreground="#282c34", 
                highlight_method='text', 
                active='#282c34', 
                inactive='#282c34', 
                this_current_screen_border='#282c34', 
                this_screen_border='#282c34', 
                font="JetBrainsMonoNL Nerd Font Regular", 
                fontsize=15, 
                hide_unused="True", 
                **powerline2
                ),
                widget.WindowName(
                background="#282c34", 
                foreground="#dfdfdf", 
                **powerline1
                ),
                widget.Systray(
                background="#282c34", 
                foreground="#dfdfdf", 
                **powerline1
                ),
                widget.TextBox(
                text='', 
                background="#c678dd", 
                foreground="#282c34", 
                fontsize=16, 
                font="JetBrainsMonoNL Nerd Font Regular"
                ),
                widget.ThermalSensor(
                background="#c678dd", 
                foreground="#282c34", 
                format='{temp:.0f}{unit}', 
                threshold=80
                ),
                widget.CPU(
                background="#c678dd", 
                foreground="#282c34", 
                format='{load_percent}%', 
                **powerline1
                ),
                widget.TextBox(
                text='', 
                background="#51afef", 
                foreground="#282c34", 
                fontsize=16, 
                font="JetBrainsMonoNL Nerd Font Regular"
                ),
                widget.Memory(
                measure_mem='G', 
                background="#51afef", 
                foreground="#282c34", 
                format = '{MemUsed:.1f}G/{MemTotal:.1f}G',
                mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e htop')}, 
                **powerline1
                ),
                widget.TextBox(
                text='', 
                background="#da8548", 
                foreground="#282c34", 
                fontsize=16, 
                font="JetBrainsMonoNL Nerd Font Regular"
                ),
                widget.Clock(
                format="%a %H:%M", 
                timezone='Europe/Belgrade', 
                background="#da8548", 
                foreground="#282c34", 
                **powerline1
                ),
                widget.TextBox(
                text='', 
                background="#98be65", 
                foreground="#282c34", 
                fontsize=16, 
                font="JetBrainsMonoNL Nerd Font Regular"
                ),
                widget.Clock(
                format="%d.%m.%Y", 
                timezone='Europe/Belgrade', 
                background="#98be65", 
                foreground="#282c34", 
                **powerline1
                ),
                widget.TextBox(text='', 
                background="#ff6c6b", 
                foreground="#282c34", 
                fontsize=16, 
                font="JetBrainsMonoNL Nerd Font Regular", 
                padding=7
                ),
                widget.KeyboardLayout(
                background="#ff6c6b", 
                foreground="#282c34", 
                configured_keyboards=['us', 'rs alternatequotes'], 
                display_map={'us': 'US', 'rs alternatequotes': 'СР'}, 
                **powerline1
                ),
                widget.TextBox(
                text='', 
                background="#e7d15f", 
                foreground="#282c34", 
                fontsize=16, 
                font="JetBrainsMonoNL Nerd Font Regular"
                ),
                widget.CheckUpdates(
                background="#e7d15f", 
                foreground="#282c34", 
                display_format="{updates} Updates", 
                no_update_string='0 Updates', 
                custom_command='(checkupdates ; yay -Qua) | cat', 
                update_interval=300, 
                colour_have_updates="#282c34", 
                colour_no_updates="#282c34", 
                mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e yay -Syyyu')},
                ),
            ],
            24,
            border_width=[0, 0, 0, 0],
            border_color=["282c34", "000000", "282c34", "000000"]
        ),
    ),
]

# Drag floating layouts.
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(border_focus=["#282c34"],
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(wm_class="galculator"), # calculator
        Match(wm_class="screengrab"), # screenshot
        Match(wm_class="oblogout"), # oblogout
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# Autostarting
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.Popen([home])

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

wmname = "LG3D"
